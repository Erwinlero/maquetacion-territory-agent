(function() {
    
    "use strict";

  $(".btn-nav-xs").click(function(e) {
      e.preventDefault();
      $("#bg-darken").toggleClass("active");
  });


  // Toggle the side navigation
  $("#sidebarToggle, #sidebarToggleTop").on('click', function(e) {
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
    if ($(".sidebar").hasClass("toggled")) {
      $('.sidebar .collapse').collapse('hide');
    };
  });


//Scroll to top
	$(document).ready(function() {
	  $(window).scroll(function() {
	    if ($(this).scrollTop() > 50) {
	      $('#back-to-top').fadeIn();
	    } else {
	      $('#back-to-top').fadeOut();
	    }
	  });
	  // scroll body to 0px on click
	  $('#back-to-top').click(function() {
	    $('body,html').animate({
	      scrollTop: 0
	    }, 400);
	    return false;
	  });
	});







/*Slider testimonials*/
        var swiper = new Swiper(".js-swiper-single-testimonials", {
          slidesPerView:1,
          spaceBetween: 10,
          pagination: {
            el: ".js-swiper-single-testimonials-pagination",
            clickable: true,
          },
          navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
          },
          breakpoints: {
            640: {
              slidesPerView: 1,
              spaceBetween: 50,
            },
            768: {
              slidesPerView: 1,
              spaceBetween: 50,
            },
            1024: {
              slidesPerView: 1,
              spaceBetween: 80,
            },
          },
        });


})();





